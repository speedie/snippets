#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc == 3 && (!strcmp(argv[1], "-c") || !strcmp(argv[1], "--celsius")) && argv[2])
        fprintf(stdout, "%d\n", (atoi(argv[2]) * 9 / 5 + 32));
    else if (argc == 3 && (!strcmp(argv[1], "-f") || !strcmp(argv[1], "--fahrenheit")) && argv[2])
        fprintf(stdout, "%d\n", (atoi(argv[2]) - 32) * 5 / 9);
    else {
        fprintf(stdout, "usage: %s [-c] [-f] degrees\n", argv[0]);
        exit(1);
    }

    return 0;
}
