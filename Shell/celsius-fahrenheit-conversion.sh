#!/bin/sh
if [ "$1" = "-c" ] || [ "$1" = "--celsius" ]; then
    printf "%d\n" "$((${2:-0} * 9 / 5 + 32))"
elif [ "$1" = "-f" ] || [ "$1" = "--fahrenheit" ]; then
    printf "%d\n" "$(((${2:-0} - 32) * 5 / 9))"
else
    printf "%d: [-c] [-f] degrees\n" "$0"
fi

